# README #

# Name and Student ID #

* Adam Jones - 16117846

# Running the progam #

* Program runs with python 3.4.
* Make sure the mybackup.py and createSignatures.py is in the same directory.
* Run the program in command prompt using the command python mybackup.py.
* Then it will wait for a command.
* The commands are:
*		init (Initializes the directory)
*		store *path to directory* (Stores the specified path)
*		list or list *pattern* (Lists the files that contained the pattern or lists all files in directory)
*		test (Gives results of testing the files in objects directory and index)
*		get *file to get* (Gets the file specified)
*		restore *directory to restore* (Restores directory to the same directory to onbjects and index file)
*		clear (Clears the back up directory)
